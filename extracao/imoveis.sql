USE casamineira_portal_20180713;

SELECT DISTINCT
    imovel.id,
    imovel.tipo_id,
    imovel.finalidade,
    imovel.area,
    imovel.quartos,
    imovel.suites,
    imovel.banheiros,
    imovel.vagas,
    imovel.latitude,
    imovel.longitude,
    (SELECT 
            COUNT(imovel_atributo.id) > 0
        FROM
            imovel_atributo
        WHERE
            imovel_atributo.imovel_id = imovel.id
                AND atributo_id = 100
                AND imovel_atributo.deletado_em IS NULL) AS area_privativa
FROM
    imovel
WHERE
    NOT EXISTS( SELECT 
            1
        FROM
            imovel_historico
        WHERE
            imovel_historico.imovel_id = imovel.id
                AND campo = 'destaque'
                AND novo = 1
        LIMIT 1)
;
