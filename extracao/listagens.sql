USE casamineira_portal_20180713;

SELECT DISTINCT
    listagem.imovel_id,
    busca.uid,
    (SELECT 
            novo
        FROM
            imovel_historico
        WHERE
            campo = 'preco'
                AND imovel_id = listagem.imovel_id
                AND imovel_historico.criado_em < IF(busca.criado_em < '2018-03-02 03:38:05',
                '2018-03-02 03:38:05',
                busca.criado_em)
        ORDER BY imovel_historico.criado_em DESC
        LIMIT 1) AS preco,
    DATE(busca.criado_em) AS listado_em
FROM
    listagem
        JOIN
    busca ON busca.id = listagem.busca_id
WHERE
    busca.uid IN (SELECT DISTINCT
            uid
        FROM
            contato_cliente
        WHERE
            contato_cliente.nome NOT LIKE '%test%'
                AND contato_cliente.email NOT LIKE '%test%'
                AND contato_cliente.email NOT LIKE '%@casamineira%'
                AND contato_cliente.uid IS NOT NULL)
        AND listagem.criado_em >= '2017-12-12 00:00:00'
;
