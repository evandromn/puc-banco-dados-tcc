USE casamineira_portal_20180713;

SELECT DISTINCT
    contato_cliente.imovel_id,
    contato_cliente.uid,
    DATE(contato_cliente.criado_em) AS listado_em
FROM
    contato_cliente
WHERE
    contato_cliente.nome NOT LIKE '%test%'
        AND contato_cliente.email NOT LIKE '%test%'
        AND contato_cliente.email NOT LIKE '%@casamineira%'
        AND contato_cliente.uid IS NOT NULL
        AND contato_cliente.criado_em >= '2017-12-12 00:00:00'
;
