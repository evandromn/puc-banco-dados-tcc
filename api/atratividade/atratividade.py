try:
  import unzip_requirements
except ImportError:
  pass

import xgboost as xgb
import pandas as pd
import json
from transformacao import Transformacao

def calcular(event, context):
    parametros = event.get('queryStringParameters', {})

    parametro_finalidade = parametros.get('finalidade')
    if parametro_finalidade not in ('venda', 'aluguel'):
        return {
            'statusCode': 400,
            'body': json.dumps({'mensagem': 'Finalidade deve ser "venda" ou "aluguel".'})
        }

    try:
        parametro_preco = int(parametros.get('preco'))
        parametro_area = int(parametros.get('area'))
        parametro_quartos = int(parametros.get('quartos'))
        parametro_suites = int(parametros.get('suites'))
        parametro_banheiros = int(parametros.get('banheiros'))
        parametro_vagas = int(parametros.get('vagas'))
        parametro_tipo_id = int(parametros.get('tipo_id'))
        parametro_latitude = float(parametros.get('latitude'))
        parametro_longitude = float(parametros.get('longitude'))
        parametro_area_privativa = int(parametros.get('area_privativa'))
    except (ValueError, TypeError):
        return {
            'statusCode': 400,
            'body': json.dumps({'mensagem': 'Parâmetro fornecido não está no formato esperado.'})
        }

    if (parametro_preco < 1 or parametro_area < 1 or parametro_quartos < 0 or
        parametro_suites < 0 or parametro_banheiros < 0 or parametro_vagas < 0  or
        parametro_tipo_id < 1):
        return {
            'statusCode': 400,
            'body': json.dumps({'mensagem': 'Parâmetro fornecido não está no intervalo esperado.'})
        }

    imoveis = pd.DataFrame([{
        'preco': parametro_preco,
        'area': parametro_area,
        'quartos': parametro_quartos,
        'suites': parametro_suites,
        'banheiros': parametro_banheiros,
        'vagas': parametro_vagas,
        'finalidade': parametro_finalidade,
        'tipo_id': parametro_tipo_id,
        'latitude': parametro_latitude,
        'longitude': parametro_longitude,
        'area_privativa': parametro_area_privativa,
    }])
    
    modelo = xgb.Booster()
    modelo.load_model('201807151414-201807152034.model')

    transformacao = Transformacao()
    imoveis_transformados = transformacao.transformar(imoveis)
    
    atratividade = modelo.predict(xgb.DMatrix(imoveis_transformados))[0]
    
    return {
        'statusCode': 200,
        'body': json.dumps({'atratividade': '{0:.9f}'.format(atratividade)})
    }