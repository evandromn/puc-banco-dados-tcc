import numpy as np
import pandas as pd

class Transformacao():
    def __init__(self):
        self.finalidades = np.array(['aluguel', 'venda'], dtype=object)
        self.tipos = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])

    def transformar(self, X):
        X_novo = self._selecionar_atributos(X)
        
        finalidades_codificadas = self._codificar_finalidades(X_novo)
        tipos_codificados = self._codificar_tipos(X_novo)
       
        X_novo = self._unir_atributos(X_novo, [
            finalidades_codificadas,
            tipos_codificados,
        ])            
        
        X_novo = self._remover_atributos(X_novo)
        
        return X_novo
    
    def _selecionar_atributos(self, X):
        colunas = [
            'tipo_id',
            'finalidade',
            'preco',
            'area',
            'quartos',
            'suites',
            'banheiros',
            'vagas',
            'latitude',
            'longitude',
            'area_privativa',
        ]

        return X[colunas].copy()
     
    def _unir_atributos(self, X, atributos):
        return X.join(atributos)
    
    def _remover_atributos(self, X):
        return X.drop(['finalidade', 'finalidade_aluguel', 'tipo_id'], axis=1)
    
    def _codificar_finalidades(self, X):
        finalidades_prefixo = 'finalidade_'
        finalidades_codificadas = pd.get_dummies(X['finalidade'], prefix=finalidades_prefixo, prefix_sep='')
        finalidades_codificadas = finalidades_codificadas.T.reindex(finalidades_prefixo + self.finalidades).T.fillna(0)        
        return finalidades_codificadas
    
    def _codificar_tipos(self, X):
        tipos_prefixo = 'tipo_id_'
        tipos_colunas = [tipos_prefixo + str(x) for x in self.tipos]
        tipos_codificados = pd.get_dummies(X['tipo_id'], prefix=tipos_prefixo, prefix_sep='')
        tipos_codificados = tipos_codificados.T.reindex(tipos_colunas).T.fillna(0)
        return tipos_codificados